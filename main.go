package main

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "gitlab.com/yuri.abram/swagger/docs"
	"log"
	"net/http"
)

type Address struct {
	Value string `json:"value"`
}

// @title Практическая задача 1.6.3
// @version 1.0
// @description  Геосервис API
// @host localhost:8080
// @BasePath /

func main() {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:8080/swagger/doc.json"),
	))

	r.Post("/api/address/search", SearchAddress)   //search.go
	r.Post("/api/address/geocode", GeocodeAddress) //geocode.go

	log.Fatalln(http.ListenAndServe(":8080", r))

}
