FROM golang:1.19-alpine AS builder

WORKDIR /app

COPY ["./", "./"]

RUN go mod download

RUN go build -o ./bin/cmd gitlab/swagger

FROM alpine

COPY --from=builder /app/bin/cmd ./

ENV PORT=8080

EXPOSE 8080

CMD ["/cmd"]